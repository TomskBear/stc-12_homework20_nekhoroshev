package service;

import pojo.Student;

import java.util.List;

public interface IStudentService {
    List<Student > getAllStudents();
    boolean addStudent(Student newStudent);

}
