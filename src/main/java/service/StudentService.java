package service;

import pojo.Student;
import repository.dao.DaoManager;
import repository.dao.student.StudentDao;
import repository.dao.student.StudentDaoImpl;

import java.util.List;

public class StudentService implements IStudentService {
    private StudentDao studentDao;

    public StudentService() {
        this.studentDao = new StudentDaoImpl(DaoManager.getInstance());
    }

    @Override
    public List<Student> getAllStudents() {
        return studentDao.getAllStudents();
    }

    public boolean addStudent(Student newStudent) {

        return studentDao.addStudent(newStudent);
    }


}
