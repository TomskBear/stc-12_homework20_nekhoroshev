package service;

import pojo.City;
import repository.dao.DaoManager;
import repository.dao.city.CityDao;
import repository.dao.city.CityDaoImpl;

public class CityService implements ICityService {
    private CityDao cityDao;

    public CityService() {
        this.cityDao = new CityDaoImpl(DaoManager.getInstance());

    }

    @Override
    public City getCityByName(String name) {
        return cityDao.getCityByName(name);
    }
}
