package service;

import pojo.City;

public interface ICityService {
    City getCityByName(String name);
}
