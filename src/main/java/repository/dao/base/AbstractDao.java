package repository.dao.base;

import repository.dao.DaoManager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class AbstractDao {
    private DaoManager manager;

    protected DaoManager getManager(){
        return manager;
    }

    protected AbstractDao(DaoManager manager) {
        this.manager = manager;
    }

    protected boolean tryExecuteStatement(PreparedStatement statement) {
        try {
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    protected ResultSet executeStatement(PreparedStatement statement) {
        try {
            return statement.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
