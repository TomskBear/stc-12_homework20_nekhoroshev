package repository.dao.base;

import repository.connectionManager.ConnectionManager;
import repository.connectionManager.ConnectionManagerJdbcImpl;
import repository.dao.DaoManager;

import java.sql.Connection;

public abstract class AbstractStatementManager {
    private static ConnectionManager connectionManager = ConnectionManagerJdbcImpl.getInstance();
    private DaoManager daoManager;

    protected AbstractStatementManager(DaoManager daoManager) {
        this.daoManager = daoManager;
    }

    protected Connection getConnection(){
        return connectionManager.getConnection();
    }

    protected DaoManager getDaoManager(){
        return daoManager;
    }
}
