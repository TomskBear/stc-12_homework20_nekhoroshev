package repository.dao;

import repository.dao.city.CityDao;
import repository.dao.city.CityDaoImpl;
import repository.dao.student.StudentDao;
import repository.dao.student.StudentDaoImpl;

public class DaoManager {
    private StudentDao studentDao;
    private CityDao cityDao;
    private static DaoManager instance;

    private DaoManager() {
        studentDao = new StudentDaoImpl(this);
        cityDao = new CityDaoImpl(this);
    }

    public static DaoManager getInstance() {
        if (instance == null) {
            instance = new DaoManager();
        }
        return instance;
    }

    public StudentDao getStudentDao() {
        return studentDao;
    }

    public CityDao getCityDao() {
        return cityDao;
    }
}
