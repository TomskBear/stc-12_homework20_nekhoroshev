package repository.dao.city;

import repository.dao.DaoManager;
import repository.dao.base.AbstractStatementManager;
import pojo.City;

import java.sql.PreparedStatement;
import java.sql.SQLException;

public class CityStatementManager  extends AbstractStatementManager {

    protected CityStatementManager(DaoManager daoManager) {
        super(daoManager);
    }

    public PreparedStatement getInsertCityStatement(City city) throws SQLException {
        PreparedStatement statement = getConnection().prepareStatement(
                "INSERT INTO cities VALUES (DEFAULT, ?, ?)");
        statement.setString(1, city.getName());
        statement.setInt(2, city.getCitizens());

        return statement;
    }

    public PreparedStatement getDeleteCityByIdStatement(int id) throws SQLException {
        PreparedStatement statement = getConnection().prepareStatement(
                "DELETE FROM cities WHERE city_id=?");
        statement.setInt(1, id);
        return statement;

    }

    public PreparedStatement getUpdateCityStatement(City city) throws SQLException {
        PreparedStatement statement = getConnection().prepareStatement(
                "UPDATE cities SET name=?, citizens=? WHERE city_id=?");

        statement.setString(1, city.getName());
        statement.setInt(2, city.getCitizens());
        return statement;
    }

    public PreparedStatement getFindCityByIdStatement(int id) throws SQLException {
        PreparedStatement statement = getConnection().prepareStatement(
                "SELECT * from cities WHERE city_id = ?");
        statement.setInt(1, id);
        return statement;
    }


    public PreparedStatement getFindCityByNameStatement(String name) throws SQLException {
        PreparedStatement statement = getConnection().prepareStatement(
                "SELECT * from cities WHERE name = ?");
        statement.setString(1, name);
        return statement;
    }
}
