package repository.dao.city;

import repository.dao.DaoManager;
import repository.dao.base.AbstractDao;
import pojo.City;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CityDaoImpl extends AbstractDao implements CityDao {
    private CityStatementManager cityStatementManager;
    private CityResultSetMapper cityResultSetMapper;

    public CityDaoImpl(DaoManager manager) {
        super(manager);
        cityStatementManager = new CityStatementManager(manager);
        cityResultSetMapper = new CityResultSetMapper();
    }

    public boolean addCity(City city) {
        try {
            return tryExecuteStatement(cityStatementManager.getInsertCityStatement(city));
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean deleteCityById(int id) {
        try {
            return tryExecuteStatement(cityStatementManager.getDeleteCityByIdStatement(id));
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean updateCity(City city) {
        if (city.getId() > 0) {
            try {
                return tryExecuteStatement(cityStatementManager.getUpdateCityStatement(city));
            } catch (SQLException e) {
                e.printStackTrace();
                return false;
            }
        }else {
            return false;
        }
    }

    public City getCityById(int id) {
        City city = null;
        try {
            ResultSet resultSet = executeStatement(cityStatementManager.getFindCityByIdStatement(id));
            if (resultSet.next()) {
                city = cityResultSetMapper.mapResultSetToCity(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return city;
    }

    public City getCityByName(String name) {
        City city = null;
        try {
            ResultSet resultSet = executeStatement(cityStatementManager.getFindCityByNameStatement(name));
            if (resultSet.next()) {
                city = cityResultSetMapper.mapResultSetToCity(resultSet);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return city;
    }
}
