package repository.dao.city;

import pojo.City;

public interface CityDao {
    public boolean addCity(City city);

    public boolean deleteCityById(int id);

    public boolean updateCity(City city);

    public City getCityById(int id);

    public City getCityByName(String name);
}
