package repository.dao.city;

import pojo.City;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CityResultSetMapper {
    public City mapResultSetToCity(ResultSet resultSet) throws SQLException {
        City city = new City();
        city.setId(resultSet.getInt("city_id"));

        city.setName(resultSet.getString("name"));
        city.setCitizens(resultSet.getInt("citizens"));
        return city;
    }
}
