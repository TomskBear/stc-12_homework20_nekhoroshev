package repository.dao.student;

import repository.dao.DaoManager;
import repository.dao.base.AbstractStatementManager;
import pojo.City;
import pojo.Student;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class StudentStatementManager extends AbstractStatementManager {

    protected StudentStatementManager(DaoManager daoManager) {
        super(daoManager);
    }

    public PreparedStatement getInsertStudentStatement(Student student) throws SQLException {
        PreparedStatement statement = getConnection().prepareStatement(
                    "INSERT INTO students VALUES (DEFAULT, ?, ?, ?, ?, ?)");
        statement.setString(1, student.getName());
        statement.setString(2, student.getSurname());
        statement.setInt(3, student.getAge());
        statement.setString(4, student.getContact());
        City city = student.getCity();
        if (city != null) {
            statement.setInt(5, city.getId());
        }
        return statement;
    }

    public PreparedStatement getStudentByIdStatement(int id) throws SQLException {
        PreparedStatement statement = getConnection().prepareStatement(
                "SELECT * from students WHERE student_id = ?");
        statement.setInt(1, id);
        return statement;
    }

    public PreparedStatement getAllStudents() throws SQLException {
        PreparedStatement statement = getConnection().prepareStatement(
                "SELECT * from students");

        return statement;
    }

    public PreparedStatement getUpdateStudentStatement(Student student) throws SQLException {
        PreparedStatement statement = getConnection().prepareStatement(
                "UPDATE students SET name=?, surname=?, age=?, contact=?, city_id=? WHERE student_id=?");

        statement.setString(1, student.getName());
        statement.setString(2, student.getSurname());
        statement.setInt(3, student.getAge());
        statement.setString(4, student.getContact());
        City city = student.getCity();
        if (city != null) {
            statement.setInt(5, city.getId());
        }

        statement.setInt(6, student.getId());
        return statement;
    }

    public PreparedStatement getDeleteStudentByIdStatement(int id) throws SQLException {
        PreparedStatement statement = getConnection().prepareStatement(
                "DELETE FROM students WHERE student_id=?");
        statement.setInt(1, id);
        return statement;
    }

    public PreparedStatement getDeleteStudentByNameStatement(String name) throws SQLException {
        PreparedStatement statement = getConnection().prepareStatement(
                "DELETE FROM students WHERE name=?");
        statement.setString(1, name);
        return statement;
    }

    public Student mapResultSetToStudent(ResultSet resultSet) throws SQLException {
        Student student = new Student();
        student.setId(resultSet.getInt("student_id"));

        student.setName(resultSet.getString("name"));
        student.setSurname(resultSet.getString("surname"));
        student.setAge(resultSet.getInt("age"));
        student.setContact(resultSet.getString("contact"));
        City city = getDaoManager().getCityDao().getCityById(resultSet.getInt("city_id"));
        student.setCity(city);
        return student;
    }

    public List<Student> mapResultSetToStudentsList(ResultSet resultSet) throws SQLException {
        List<Student> result = new ArrayList<>();
        while(resultSet.next()) {
            result.add(mapResultSetToStudent(resultSet));
        }
        return result;
    }
}
