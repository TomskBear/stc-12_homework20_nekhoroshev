package my.first.servlets.controllers;

import pojo.City;
import pojo.Student;
import service.CityService;
import service.ICityService;
import service.IStudentService;
import service.StudentService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class StudentServlet extends HttpServlet {

    private IStudentService studentService;
    private ICityService cityService;


    @Override
    public void init() throws ServletException {
        super.init();
        studentService = new StudentService();
        cityService = new CityService();
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        List<Student> students = studentService.getAllStudents();
        req.setAttribute("list", students);
        req.getRequestDispatcher("/students.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String studentName = req.getParameter("name");
        String studentSurname = req.getParameter("surname");
        String studentAge = req.getParameter("age");
        String studentContact = req.getParameter("contact");
        String studentCity = req.getParameter("city");

        Student newStudent = new Student();
        newStudent.setName(studentName);
        newStudent.setSurname(studentSurname);
        if (studentAge != null) {
            newStudent.setAge(Integer.parseInt(studentAge));
        }
        newStudent.setContact(studentContact);
        City city = cityService.getCityByName(studentCity);
        if (city != null) {
            newStudent.setCity(city);
        }
        studentService.addStudent(newStudent);

        List<Student> students = studentService.getAllStudents();
        req.setAttribute("list", students);
        req.getRequestDispatcher("/students.jsp").forward(req,resp);
    }
}
